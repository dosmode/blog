---
title: "About"
description: "Transparency"
date: 2019-08-07T13:37:00+00:00
draft: false
---

# Announcement

I'm currently looking for a (part-time) job in the area of Lisbon, Portugal. Please check out my <a href="/CV_Mark_Schmitz_2019-08.pdf">CV [PDF ~190 KB]</a> (or as <a href="/CV_Mark_Schmitz_2019-08.svg">SVG ~150 KB</a>) to get an idea what I could help you with – and have a look at the latest <a href="/posts">blog posts</a>

# Gist

On this blog I'm mainly writing about IT/software projects around web technology, browsers & sites (development and analysis), data protection, and other random stuff.

The content is editorially independent, sources will be cited whenever possible, and emphasis is put on documentation and reproducibility.

## Rule of Transparency

First of all, if any of these points (especially about data collection & protection) change in the future I will strive to update the principles and policies stated here.

### Privacy policy

* no third-party resources
* no paid advertisement or sponsored content
* no sharing of data with (third party) providers
* no web analytics or user tracking

### Why the name DOSmo.de

* I purchased the domain in a sale and thought it's a funny play on words, as before Windows there was MS-DOS and such and the free RAM subtitle on the landing page is a reference on how hard it used to be to get close to 640 KB (Kilobyte) free RAM.
* Also, now my aim is to keep each page below 632 KB size/transfer volume total