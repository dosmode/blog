---
title: "Backend"
description: "Docker: Traefik & nginx"
date: 2019-08-11T23:59:59+01:00
draft: false
---

# What's serving these web pages?

## Backend: Docker containers running traefik & ngingx.

The output of a static site generator is (by definition) static HTML + CSS + JavaScript. So you can host it on any web hosting service (even as [GitHub Pages](https://pages.github.com/) or [GitLab pages](https://about.gitlab.com/product/pages/), where you could also run the Hugo build process) or set up a simple (but secure) web server.  

I chose to use containerized versions of [nginx](https://nginx.org/) as webserver and [Traefik](https://traefik.io/) as reverse proxy. Although nginx can also be used as reverse proxy, Traefik seems to play much more nicely with (adding) different Docker containers.

The whole setup runs on a VPS (virtual private server) in Docker containers using the official [Traefik:1.7](https://hub.docker.com/_/traefik) Docker image (which interestingly is a [minimal `scratch` image](https://docs.docker.com/develop/develop-images/baseimages/) image – see also e.g. [their amd64 Dockerfile](https://github.com/containous/traefik-library-image/blob/master/scratch/amd64/Dockerfile)) and the official [nginx:alpine](https://hub.docker.com/_/nginx) Docker image.

Below you can find my configuration files; make sure to enabe gzip compression only in one place as Traefik will compress everything (even e.g. jpegs and woff2) it's probably better to enable it in the nginx config.

Below you'll find the content of the `docker-compose.yml` file. The webserver content is mounted read-only and resides on the ramdisk (`/dev/shm`) on the host (for fastest load times and no need for a RAM-cache scheme as long as the whole thing is rather small).  
A maybe unexpected default behaviour/setting of `systemd` will remove all "user content" in `/dev/shm` upon ssh logout, make sure to change that as described <a href="https://askubuntu.com/q/884127">here</a> by adding the line `RemoveIPC=no` to `/etc/systemd/logind.conf`.

```yaml
version: '3'

services:
  reverse-proxy:
    image: traefik:1.7 # The official Traefik docker image, currently 1.7.13
    restart: always
    command: --configFile=/config/traefik_1.7.toml --api --docker --sendAnonymousUsage=false # Enables the web UI, listen to docker
    ports:
      - "80:80"       # The HTTP port
      - "443:443"     # The HTTPS port
      - "8080:8080"   # The Web UI (enabled by --api)
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro # So that Traefik can listen to the Docker events
      - ./config:/config
    networks:
      - web
    labels:
      - "traefik.enable=false"
    # container_name: traefik_1.7.13

  whoami:
    # A container that exposes an API to show its IP address
    image: containous/whoami
    networks:
      - web
    labels:
      - "traefik.docker.network=web"
      - "traefik.enable=true"
      - "traefik.basic.frontend.rule=Host:ip.dosmo.de"
      - "traefik.basic.port=80"
      - "traefik.basic.protocol=http"

  web:
    image: nginx:alpine # The Official build of Nginx. Currently 1.17.2
    networks:
      - web
    volumes:
      - /dev/shm/public:/usr/share/nginx/html:ro
      - ./config/nginx.conf:/etc/nginx/nginx.conf:ro
    labels:
      - "traefik.docker.network=web"
      - "traefik.enable=true"
      - "traefik.basic.frontend.rule=Host:dosmo.de,www.dosmo.de"
      - "traefik.basic.port=80"
      - "traefik.basic.protocol=http"

networks:
  web:
    external: true
```

Below is my `traefik_1.7.toml` file, which is pretty standard and enables the web api for now with a [HTTP basic access authentication](https://en.wikipedia.org/wiki/Basic_access_authentication) protection (I will probably change this soon, as it seems unnecessary insecure – someone could e.g. intercept the credentials on a public WiFi).

```toml
debug = false
sendAnonymousUsage = false

logLevel = "ERROR"
defaultEntryPoints = ["https", "http"]

[entryPoints]
  [entryPoints.http]
  address = ":80"
  # compress = true # do not enable gzip compression both in nginx AND traefik - also, traefik will compress everyting!
    [entryPoints.http.redirect]
    entryPoint = "https"
  [entryPoints.https]
  address = ":443"
  # compress = true # do not enable gzip compression both in nginx AND traefik - also, traefik will compress everyting!
    [entryPoints.https.tls]
  [entryPoints.traefik]
  address = ":8080"
    [entryPoints.traefik.auth]
      [entryPoints.traefik.auth.basic]
        usersFile = "/config/secret/.htpasswd"

[retry]

[api]
entryPoint = "traefik"

[docker]
endpoint = "unix:///var/run/docker.sock"
domain = "dosmo.de"
watch = true
exposedByDefault = false

[acme]
email = "user@host.tld"
storage = "/config/secret/acme.json"
entryPoint = "https"
onHostRule = true

  [acme.httpChallenge]
  entryPoint = "http"
```

The nginx configuration is still missing the `Cache-Control` settings. Once it's finished, I'll add it here.

## Update 2019-08-22: 

### All resources to create and serve this website are available on [gitlab/dosmode](https://gitlab.com/dosmode)

See also the post about the [tools and docker images]({{< relref "tools-and-docker-images.md" >}}) used for DOSmo.de

<script src="/js/prism.js"></script>