---
title: "Virtualization on VPS"
description: "needs VT-X or AMD-V"
date: 2099-10-10T23:59:59+01:00
draft: true
---

```bash
# SSH -X “Warning: untrusted X11 forwarding setup failed: xauth key data not generated”

# https://stackoverflow.com/a/48543215/1097752

adduser gui # <- this is interactive!
usermod -aG sudo gui

apt-get clean && apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y

apt-get install htop nethogs xauth xorg -y

# apt-get install openbox
# or 
apt-get install xfce4 -y
# xfce4-goodies dbus-x11 x11-xserver-utils # skipped
# or
# apt-get install lxde

apt-get install xrdp xorgxrdp -y

echo "startxfce4" > ~/.Xclients
chmod +x ~/.Xclients

apt-get install pcsc-tools

apt install gcc build-essential libncursesw5 linux-headers-$(uname -r) -y

curl -JOL https://www.vmware.com/go/getplayer-linux

First steps on a Linux (Debian) server:

First things first, Add your user, Require ssh key authentication, Test deploy user and setup sudo, Enforce ssh key logins, Setting up a firewall, Automated security updates, fail2ban, 2 Factor Authentication, Logwatch, All done

Install Fail2ban
Require public key authentication
Test The New User & Enable Sudo
Lock Down SSH
Set Up A Firewall
Enable Automatic Security Updates
Install Logwatch To Keep An Eye On Things
All Done!
Update

1) Your first couple minutes on a server should be used to install a configuration management client, if your bootstrap policies somehow don't already install one.

2) Everything else listed in this document should be configured by a configuration management system.

3) "User account sync tools" should have no place in a modern infrastructure, you should use your configuration management tool to (at the bare minimum) deploy /etc/passwd and /etc/sudoers across your infrastructure.

4) You should not use shared/role accounts. The "incremental cost" is paid back immediately when someone leaves your organization; having to update everyone of a changed password or having a password change have any negative impact at all should not be a thing your company does.

This stuff isn't hard. It's worth doing right.

The problem is that most of the recommendations are bad.
Having an up-to-date system and only accepting security updates is a good policy. Fail2ban is a good tool (but it's a starting point; you should be doing other things to detect suspicious behavior).

The rest is just bad advice.

Having everyone log in using a single user account is a terrible idea. You can't audit who did what, ever. You have to remember to remove people from authorized_keys when they leave, and also make sure that they haven't left themselves a backdoor -- a cron job that reinstates the key, an extra user account, even just changing the root password to something else (how often do you actually check the root password on your boxes?).

User account management is a pain, so that's why we have things like LDAP. Everyone has their own user account. You can audit who does what on every machine, and for stuff that requires root, sudo will log the things people do (of course, if you let people have root shells, that's harder). The only people who get access to a local account (and/or root, but I still think root should just have a random password that no one knows) are a few sysadmins. When someone leaves, you kill their account in the LDAP server.

Even better, if this is a possibility, put up a VPN, and only allow ssh access via the VPN (using a firewall). Tie the VPN login to LDAP (and don't let non-VPN-admins ssh directly to the VPN server), and then you can be sure that without a user account in LDAP, no one can log into your servers.

Blind-updating systems in production is a terrible idea. Things break in the open source world all the time when you do this. Never ever use unattended-upgrades. You just need to be on top of security updates. Period. No excuses.

You should never even have a "my first five minutes on a server" type thing anyway. Rolling out a new server should be fully automatically operationalized. The first time you log into the server, it should be completely ready to go. It should be ready to go without you needing to log into it at all. This takes a small amount of up-front effort, and will pay off immediately when you bring up your second server.

Puppet, chef, cfengine, ansible, and salt are a few.

  
Firstly, a nice checklist. Easy actionable steps, repeatable, and pretty much most of what you need.
Secondly, you are about 4-5 hours away from learning puppet (or Chef) and making this checklist into actual code.

Thirdly, you now have a checklist of items that you can use in a job interview if you get the oppertunity to gain a new-hire or an intern.

Lastly, good on you for submitting this to a peer-review on HN. We can be a picky lot.

TL;DR Checklists are a good first step for building a proper config management system.

apt-get upgrade