---
title: "CSS animations"
description: "@keyframes"
date: 2019-08-18T23:59:59+01:00
draft: false
---

# Building an animated 404 page.

## Sophisticated animations with CSS only? Not so much…

When using CSS `@keyframes` based animations, keep it simple and use JavaScript instead for advanced timing. 
There is this amazing [Scared yeti 404 page](https://www.reddit.com/r/web_design/comments/985oea/scared_yeti_404_page/) that was posted on [reddit/r/web_design](https://www.reddit.com/r/web_design) some time ago. But it was done using the [GreenSock Animation Platform (GSAP)](https://greensock.com/), which is a suite of tools for scripted animation.

GSAP comes with with a quite permissive free license and some required JavaScript to make it all work (for the [codepen.io example](https://codepen.io/dsenneff/pen/mjZgmN): 36.5 KB for `TweenMax.min.js` and 19.8 KB for `MorphSVGPlugin.min.js`).

In my naivety I thought this can be recreated using only CSS animations and the [`@keyframes`](https://developer.mozilla.org/en-US/docs/Web/CSS/@keyframes) rules to make something similar to the scripted (GSAP) animations. 
Well, it turns out I was pretty wrong as using only [CSS animations](https://drafts.csswg.org/css-animations) is probably a task in vain when trying produce or reproduce advanced (timed/scripted) animations.

Half a day was eventually spent just to make this very poor [clone](/404.html) using `@keyframes` in CSS only. But it's OK for me for now and I updated the `nginx.conf` file to display that page for the [HTTP status code](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) 404.