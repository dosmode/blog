---
title: "Reduce glyphs in web-fonts"
description: "using fontTools"
date: 2019-08-30T23:59:59+01:00
draft: false
---

# The pyftsubset command creates a glyph subset.

[fontTools](https://github.com/fonttools/fonttools) is a Python library for manipulating fonts and it comes with commands for merging and subsetting fonts, specifically [`pyftsubset`](https://github.com/fonttools/fonttools/blob/master/Lib/fontTools/subset/__init__.py) can be used to only include a subset of glyphs in a new font-file.

As I wrote in my [initial post]({{< relref "init.md" >}}#all-resources-1-sup-st-sup-party) I'm using the '[Iosevka](https://typeof.net/Iosevka/)' font for mono-spaced text and this font comes with [a lot of glyphs (3728 in v2.3.0)](http://torinak.com/font/lsfont.html#https://typeof.net/Iosevka/iosevka/woff/iosevka-light.woff) and thereby is quite heavy in size (113 kb for e.g. Iosevka Light Regular in the [woff2](https://github.com/google/woff2) format).

The [GitHub issue #238](https://github.com/be5invis/Iosevka/issues/238) in the Iosevka repository describes a way to create a smaller webfont containing only the desired subset of glyphs.

In oder to be able to use this on different operating systems and not to install more dependencies locally I have created a Docker image containing the 4.0.0 [release](https://github.com/fonttools/fonttools/releases) of fontTools with support for [`zopfli`](https://github.com/google/zopfli) compressed woff (saves additionally a few percent in comparison with `gzip`) and woff2 ([`brotli`](https://github.com/google/brotli) compressed).

You can find the [Dockerfile](https://gitlab.com/dosmode/tools/blob/master/fonttolls_Docker_image/fonttools.Dockerfile) for my fontTool image on [gitlab.com/DOSmode/tools](https://gitlab.com/dosmode/tools) in the `fonttolls_Docker_image` folder. I strongly encourage you to build you own Docker image from that, but if you're feeling lazy and adventurous you can also grab it pre-made from [Docker Hub](https://hub.docker.com/u/dosmode).

My GitLab.com/DOSmode/tools project also contains some sample files for subsetting the glyphs. In my current implementation I've used the `chars_medium.txt` file containing 399 glyphs.

My workflow was the following: 

* Switch to the folder containing the file for the glyphs, which will be subset, and the font-files in woff format in a sub-directory. Run a Docker container based on my fontTools image: `docker run -it --rm -h fontTools --name fonttools -v ${PWD}:/opt dosmode/fonttools`, with interactive terminal and with a bind-mount of the current directory to `/opt` in the container.
* Inside the container:
  * Switch to the sub-directory containing the fonts and create the folder `subset_medium` with `mkdir subset_medium`
  * Run two small bash loops to create both the woff and the woff2 format:
  ```bash
  # for woff
  for f in *.woff ; do pyftsubset $f --text-file=../chars_medium.txt --layout-features+=onum,pnum,liga --name-IDs+=0,4,6,8,9,10 --verbose --flavor=woff --with-zopfli --output-file=subset_medium/$f ; done
  # and for woff2
  for f in *.woff ; do pyftsubset $f --text-file=../chars_medium.txt --layout-features+=onum,pnum,liga --name-IDs+=0,4,6,8,9,10 --verbose --flavor=woff2 --output-file=subset_medium/${f}2 ; done
  ```

The result of this is a smaller version (e.g. ~21 KB for `iosevka-ss04-light.woff2`) of the Iosevka font containing only the [specified 399 glyphs](http://torinak.com/font/lsfont.html#https://dosmo.de/fonts/subset_399glyphs/v2.3.0/iosevka-ss04-light.woff).

Also, a big shout-out to the web page [List glyphs in font](http://torinak.com/font/lsfont.html) by Jan Bobrowski (CC BY-SA 3.0), which is used in the links above to display the glyphs of the font-files.

<script src="/js/prism.js"></script>