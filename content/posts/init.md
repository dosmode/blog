---
title: "Init…"
description: "What took you so long?"
date: 2019-08-10T23:59:59+01:00
draft: false
---

## Yes, hello there! – Finally.

Now, beginning of August 2019, my web-site goes online at last – one month later than I initially planed. What took so long? 

### One month longer

The initial idea was to use a static site generator with the ["Clean Blog"](https://blackrockdigital.github.io/startbootstrap-clean-blog/index.html) theme based on [Bootstrap 4](https://getbootstrap.com/). I had used the Ruby-based static site generator [Jekyll](https://jekyllrb.com/) before for a rather small project and already noticed the build times (i.e. generating the static HTML pages from the template and the content). Therefore, I decided to switch to the Go-based [Hugo](https://gohugo.io/) framework, which states that it is:

> The world’s fastest framework for building websites.

The [Hugo documentation](https://gohugo.io/documentation/) is rather sizable and could maybe provide a better overview or linkage among the sections, the [accompanying videos](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) (hosted on YouTube, last updated in September 2017) do help – although they seem to have a rather low view count (only around 10k per video on average).

## Too many bytes

The whole thing with the "Clean Blog" theme and Bootstrap 4 includes was then more than 1 MB uncompressed __without__ images. Although the minified gzipped version are a lot smaller in size (jQuery ~30 KB, bootstrap css ~23 KB + js ~22 KB, font-awesome ~159 KB) I decided to build my own style-sheet (using [SASS](https://www.sass-lang.com/)) and skip on jQuery and Bootstrap 4 JavaScript – as this would only be used to unfold the menu on mobile displays (and show the nav-bar when scrolling up, where I'm not sure if I actually like that feature…)  
I'll write more about my Hugo/SASS (+PostCSS etc.) work-flow in a future post.

### All resources 1<sup>st</sup> party

As stated in the [about](/about) section I want this web-site to be as privacy-friendly as possible, which also means no third-party resources. The [google-webfonts-helper](https://google-webfonts-helper.herokuapp.com/fonts/merriweather) service was used to quickly obtain the resources necessary to self-host the fonts 'Muli' and 'IBM Plex Serif' from Google Fonts; and the great '[Iosevka](https://typeof.net/Iosevka/)' font is used for mono-spaced text. The main issue with the default font files (especially Iosevka) is that they attribute significantly to the transfer volume of the website: `iosevka-light.woff2` alone is already 114 KB. In [a future post]({{< relref "glyph-reduction-with-fonttools.md" >}}) I'll describe how to reduce the size by limiting the character set using [fonttools: pyftsubset](https://github.com/fonttools/fonttools). For syntax highlighting I settled on using a customized style-sheet with [Prism](https://prismjs.com/), which is using JavaScript to generate the highlighting classes in the client browser, and in a future post I'll explain why and maybe try to fix Hugo's highlighting.

### Shout-outs

A few further sources of inspiration and services used:

* [RealFaviconGenerator](https://realfavicongenerator.net/)
* [Paletton.com](http://paletton.com)
* [Advanced CSS course](https://github.com/jonasschmedtmann/advanced-css-course) and [further resources](http://codingheroes.io/resources/)
* [Kuketz IT-Security Blog (in German)](https://www.kuketz-blog.de/) – minimal transfer volume with functional and pleasant design (using WordPress?)
* [Vincent Bernat's blog and font tips](https://vincent.bernat.ch/en/blog/2018-more-privacy-blog#fonts)
