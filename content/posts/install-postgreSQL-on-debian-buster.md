---
title: "PostgreSQL 12"
description: "on Debian Buster (10.x)"
date: 2019-10-02T23:59:59+01:00
draft: false
---

# Use the PostgreSQL Apt Repository to install the latest version.

The PostgreSQL apt repository supports the current stable versions of Debian – see also [Linux downloads (Debian)](https://www.postgresql.org/download/linux/debian/) and the [PostgreSQL wiki](https://wiki.postgresql.org/wiki/Apt). For Debian Buster (10.x) add the following line to either `/etc/apt/sources.list.d/pgdg.list` or `/etc/apt/sources.list`

```bash
# add to e.g. /etc/apt/sources.list.d/pgdg.list
deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main
```

Then update the apt-cache and install PostgreSQL 12 by running (as root or via sudo):

```bash
# run as root via e.g. sudo -i or prepend each command with sudo
apt update
apt install -t buster-pgdg postgresql-12
```

Example output

```text
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  libpq5 pgdg-keyring postgresql-client-12 postgresql-client-common postgresql-common sysstat
Suggested packages:
  postgresql-doc-12 libjson-perl isag
The following NEW packages will be installed:
  libpq5 pgdg-keyring postgresql-12 postgresql-client-12 postgresql-client-common postgresql-common sysstat
0 upgraded, 7 newly installed, 0 to remove and 0 not upgraded.
Need to get 16.9 MB of archives.
After this operation, 55.1 MB of additional disk space will be used.
Do you want to continue? [Y/n] 
Get:1 http://apt.postgresql.org/pub/repos/apt buster-pgdg/main amd64 libpq5 amd64 12.0-1.pgdg100+1 [172 kB]
Get:2 http://mirror.hetzner.de/debian/packages buster/main amd64 sysstat amd64 12.0.3-2 [562 kB]
Get:3 http://apt.postgresql.org/pub/repos/apt buster-pgdg/main amd64 pgdg-keyring all 2018.2 [10.7 kB]
Get:4 http://apt.postgresql.org/pub/repos/apt buster-pgdg/main amd64 postgresql-client-common all 207.pgdg100+1 [85.8 kB]
Get:5 http://apt.postgresql.org/pub/repos/apt buster-pgdg/main amd64 postgresql-client-12 amd64 12.0-1.pgdg100+1 [1,270 kB]
Get:6 http://apt.postgresql.org/pub/repos/apt buster-pgdg/main amd64 postgresql-common all 207.pgdg100+1 [235 kB]
Get:7 http://apt.postgresql.org/pub/repos/apt buster-pgdg/main amd64 postgresql-12 amd64 12.0-1.pgdg100+1 [14.6 MB]
Fetched 16.9 MB in 0s (49.7 MB/s)                                     
Preconfiguring packages ...
Selecting previously unselected package libpq5:amd64.
(Reading database ... 77157 files and directories currently installed.)
Preparing to unpack .../0-libpq5_12.0-1.pgdg100+1_amd64.deb ...
Unpacking libpq5:amd64 (12.0-1.pgdg100+1) ...
Selecting previously unselected package pgdg-keyring.
Preparing to unpack .../1-pgdg-keyring_2018.2_all.deb ...
Unpacking pgdg-keyring (2018.2) ...
Selecting previously unselected package postgresql-client-common.
Preparing to unpack .../2-postgresql-client-common_207.pgdg100+1_all.deb ...
Unpacking postgresql-client-common (207.pgdg100+1) ...
Selecting previously unselected package postgresql-client-12.
Preparing to unpack .../3-postgresql-client-12_12.0-1.pgdg100+1_amd64.deb ...
Unpacking postgresql-client-12 (12.0-1.pgdg100+1) ...
Selecting previously unselected package postgresql-common.
Preparing to unpack .../4-postgresql-common_207.pgdg100+1_all.deb ...
Adding 'diversion of /usr/bin/pg_config to /usr/bin/pg_config.libpq-dev by postgresql-common'
Unpacking postgresql-common (207.pgdg100+1) ...
Selecting previously unselected package postgresql-12.
Preparing to unpack .../5-postgresql-12_12.0-1.pgdg100+1_amd64.deb ...
Unpacking postgresql-12 (12.0-1.pgdg100+1) ...
Selecting previously unselected package sysstat.
Preparing to unpack .../6-sysstat_12.0.3-2_amd64.deb ...
Unpacking sysstat (12.0.3-2) ...
Setting up pgdg-keyring (2018.2) ...
Removing apt.postgresql.org key from trusted.gpg: OK
Setting up libpq5:amd64 (12.0-1.pgdg100+1) ...
Setting up sysstat (12.0.3-2) ...

Creating config file /etc/default/sysstat with new version
update-alternatives: using /usr/bin/sar.sysstat to provide /usr/bin/sar (sar) in auto mode
Created symlink /etc/systemd/system/multi-user.target.wants/sysstat.service → /lib/systemd/system/sysstat.service.
Setting up postgresql-client-common (207.pgdg100+1) ...
Setting up postgresql-client-12 (12.0-1.pgdg100+1) ...
update-alternatives: using /usr/share/postgresql/12/man/man1/psql.1.gz to provide /usr/share/man/man1/psql.1.gz (psql.1.gz) in auto mode
Setting up postgresql-common (207.pgdg100+1) ...
Adding user postgres to group ssl-cert

Creating config file /etc/postgresql-common/createcluster.conf with new version
Building PostgreSQL dictionaries from installed myspell/hunspell packages...
Removing obsolete dictionary files:
Created symlink /etc/systemd/system/multi-user.target.wants/postgresql.service → /lib/systemd/system/postgresql.service.
Setting up postgresql-12 (12.0-1.pgdg100+1) ...
Creating new PostgreSQL cluster 12/main ...
/usr/lib/postgresql/12/bin/initdb -D /var/lib/postgresql/12/main --auth-local peer --auth-host md5
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/12/main ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Europe/Berlin
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

Success. You can now start the database server using:
 pg_ctlcluster 12 main start

Ver Cluster Port Status Owner    Data directory              Log file
12  main    5432 down   postgres /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log
update-alternatives: using /usr/share/postgresql/12/man/man1/postmaster.1.gz to provide /usr/share/man/man1/postmaster.1.gz (postmaster.1.gz) in auto mode
Processing triggers for systemd (241-7~deb10u1) ...
Processing triggers for man-db (2.8.5-2) ...
Processing triggers for libc-bin (2.28-10) ...
```

To install [pgAdmin](https://www.pgadmin.org/) check out the following section and consider maybe putting that installation inside a container or using the [official Docker image for pgAdmin](https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html).

```bash
apt install -t buster-pgdg pgadmin4
```

Example output

```text
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  alembic fonts-font-awesome fonts-open-sans fonts-roboto-unhinted javascript-common libdouble-conversion1 libevdev2 libinput-bin libinput10 libjs-jquery libjs-sphinxdoc libjs-underscore libmtdev1 libpcre2-16-0 libqt5core5a libqt5dbus5 libqt5gui5 libqt5network5 libqt5svg5 libqt5widgets5 libwacom-bin libwacom-common libwacom2 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-randr0 libxcb-render-util0 libxcb-xinerama0 libxcb-xkb1 libxkbcommon-x11-0 pgadmin4-common pgadmin4-doc python-babel-localedata python3-alembic python3-babel python3-bcrypt python3-blinker python3-click python3-colorama python3-dateutil python3-editor python3-flask python3-flask-babelex python3-flask-gravatar python3-flask-htmlmin python3-flask-login python3-flask-mail python3-flask-migrate python3-flask-paranoid python3-flask-principal python3-flask-security python3-flask-sqlalchemy python3-flaskext.wtf python3-htmlmin python3-itsdangerous python3-jinja2 python3-mako python3-markupsafe python3-nacl python3-openssl python3-paramiko python3-passlib python3-psutil python3-psycopg2 python3-pyasn1 python3-pyinotify python3-simplejson python3-sqlalchemy python3-sqlalchemy-ext python3-sqlparse python3-sshtunnel python3-tz python3-werkzeug python3-wtforms qt5-gtk-platformtheme qttranslations5-l10n
Suggested packages:
  apache2 | lighttpd | httpd qt5-image-formats-plugins qtwayland5 python-blinker-doc python-flask-doc python-flask-login-doc python-jinja2-doc python3-beaker python-mako-doc python-nacl-doc python-openssl-doc python3-openssl-dbg python3-gssapi python-psutil-doc python-psycopg2-doc python-pyinotify-doc python-sqlalchemy-doc python3-mysqldb python3-fdb python-sqlparse-doc ipython3 python3-lxml python3-termcolor python3-watchdog python-werkzeug-doc python3-django python3-django-localflavor
The following NEW packages will be installed:
  alembic fonts-font-awesome fonts-open-sans fonts-roboto-unhinted javascript-common libdouble-conversion1 libevdev2 libinput-bin libinput10 libjs-jquery libjs-sphinxdoc libjs-underscore libmtdev1 libpcre2-16-0 libqt5core5a libqt5dbus5 libqt5gui5 libqt5network5 libqt5svg5 libqt5widgets5 libwacom-bin libwacom-common libwacom2 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-randr0 libxcb-render-util0 libxcb-xinerama0 libxcb-xkb1 libxkbcommon-x11-0 pgadmin4 pgadmin4-common pgadmin4-doc python-babel-localedata python3-alembic python3-babel python3-bcrypt python3-blinker python3-click python3-colorama python3-dateutil python3-editor python3-flask python3-flask-babelex python3-flask-gravatar python3-flask-htmlmin python3-flask-login python3-flask-mail python3-flask-migrate python3-flask-paranoid python3-flask-principal python3-flask-security python3-flask-sqlalchemy python3-flaskext.wtf python3-htmlmin python3-itsdangerous python3-jinja2 python3-mako python3-markupsafe python3-nacl python3-openssl python3-paramiko python3-passlib python3-psutil python3-psycopg2 python3-pyasn1 python3-pyinotify python3-simplejson python3-sqlalchemy python3-sqlalchemy-ext python3-sqlparse python3-sshtunnel python3-tz python3-werkzeug python3-wtforms qt5-gtk-platformtheme qttranslations5-l10n
0 upgraded, 78 newly installed, 0 to remove and 0 not upgraded.
Need to get 42.1 MB of archives.
After this operation, 143 MB of additional disk space will be used.
```

<script src="/js/prism.js"></script>