---
title: "Remote Linux GUI"
description: "using xrdp"
date: 2019-09-04T23:59:59+01:00
draft: false
---

# Connect via RDP to a lightweight desktop environment Xfce.

Sometimes it's nice to have a graphical user interface (GUI) for testing things or running applications that require a GUI. This is a quick write-up of the necessary steps on a Linux (Debian or Ubuntu) server or virtual private server (VPS). If you plan on running this in production you probably wanna harden you server first and maybe expose [xrdp](http://xrdp.org/) ([github.com/neutrinolabs/xrdp](https://github.com/neutrinolabs/xrdp)) on `localhost` only and connect via VPN (e.g. [Wireguard](https://www.wireguard.com/)) and put all of this in an system/configuration management setup ([see this comparison on wikipedia](https://en.wikipedia.org/wiki/Comparison_of_open-source_configuration_management_software))

Also note that there are valid arguments against a GUI on a server:

* More code subject to security vulnerabilities, more packages that need updating, and more server downtime.
* Performance suffers because resources are consumed by the GUI.
* It is best practice to only install needed software on a production server.
* The GUI may include other network services that are inappropriate for a server.

Source: [Ubuntu ServerGUI wiki](https://help.ubuntu.com/community/ServerGUI)

The steps below include: 

 * Updating the packet manager
 * Installing all available updates (you may wanna check if you actually want to do upgrade all – I'm assuming here a freshly started VPS).
 * Installing the [X.Org](https://www.x.org/wiki/) open source implementation of the X Window System, [Xfce](https://xfce.org/) as a desktop environment ([Openbox](http://openbox.org/wiki/Main_Page) or [Fluxbox](http://www.fluxbox.org/) would be an even slimmer alternative window managers, or [lxde](https://lxde.org/), or whatever you prefer)
  * Lastly, you should probably not be running the GUI as root, so add an user (here: "gui") if you haven't done so yet. And for that user create a `~/.Xclients` file (here: `/home/gui/.Xclients`) that will be executed upon RDP connection and start your window manager and GUI. See [this stackoverflow answer](https://askubuntu.com/a/289672) for other examples.
  
\

```bash
apt-get update && apt-get dist-upgrade -y
# this will install the Xfce as Desktop Environment
apt-get install xorg xrdp xorgxrdp xfce4 -y

# on ubuntu you may want to add xubuntu-icon-theme
# apt-get install xubuntu-icon-theme -y

adduser gui # <- this is interactive! and will create a new user named "gui"
usermod -aG sudo gui # this is optional and adds user "gui" to the sudo group

# either switch to the user, or change ownership of their .Xclients file
# su - gui

echo "startxfce4" > /home/gui/.Xclients
chmod +x /home/gui/.Xclients
chown gui:gui /home/gui/.Xclients # you could also leave the owner:group as root:root
```

Now you can connect to your server and interact with the GUI via RDP (Microsoft Remote Desktop Protocol). From a macOS machine you can use `brew install freerdp` to install  or the [Microsoft Remote Desktop 10](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466) available on the Mac App Store or check out [these alternatives](https://apple.stackexchange.com/q/2862), from Windows you should have the [Remote Desktop Connection](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients) and from Linux maybe [Remmina](https://remmina.org/).

I found the RDP connection more responsive than X-Window forwarding. Also, for X-window forwarding and for `xfreerdp` you will need a local X-Server like [XQuartz](https://www.xquartz.org/) for macOS and to properly forward via SSH add `XAuthLocation /opt/X11/bin/xauth` to your `~/.ssh/config` file as mentioned [here](https://stackoverflow.com/a/48543215)

# Direct RDP connections are not secure

Again, this was for testing purposes only. You do want to secure your connection using a VPN or SSH and have xrdp listen on on localhost only (and check your firewall)!


## Update 2019-09-05: Allow access only via ssh tunnel

On Debian 10 (Buster) this __did not work__ (i.e. no RDP connection over tunneled SSH was possible):

* ~~Edit /etc/xrdp/[`xrdp.ini`](http://manpages.ubuntu.com/manpages/bionic/man5/xrdp.ini.5.html) and add the line `address=127.0.0.1` to make xrdp listen only on localhost.~~
  * 
```bash
# this should work, but somehow did not…
# edit /etc/xrdp/xrdp.ini
# add line in [Globals] section
# address=127.0.0.1
# service xrdp restart 
```

Instead, configure your firewall to block by all default and only allow your desired services/ports, e.g. ssh for a fresh VPS.

```bash
apt install ufw
ufw allow ssh # make sure you didn't change your ssh port, as this defaults to port 22
ufw logging on
sudo ufw enable
sudo ufw status
```

This sets up a default deny (DROP) firewall for incoming connections, with all outbound connections allowed with state tracking, see [ubuntu wiki](https://wiki.ubuntu.com/UncomplicatedFirewall).

If you've changed the [port number that `sshd` listens on](https://manpages.debian.org/buster/openssh-server/sshd_config.5.en.html#Port) you must issue a different `ufw` command – as `ufw` does not (yet) get the actual ssh port:

```bash
# in case of a non-standard ssh port, e.g. not 22 but 65522
ufw allow 65522/tcp comment 'Open port 65522 where sshd should be listening, check /etc/ssh/sshd_config'
ufw logging on
sudo ufw enable 
ufw status verbose # check ufw status 
```

Add your ssh public key to the user "gui", i.e. create the folder `/home/gui/.ssh` and add your public ssh key e.g. from `id_rsa.pub` to the file `/home/gui/.ssh/authorized_keys` (also maybe [disable password based ssh login](https://manpages.debian.org/buster/openssh-server/sshd_config.5.en.html#PasswordAuthentication)).

Then, on the machine where you run your RDP client create an SSH tunnel, forwarding the RDP port.

```bash
# run this on you client 
ssh -L 3389:127.0.0.1:3389 -C -N -l gui $your_server_ip
```

Then connect with your RDP client to `127.0.0.1:3389` and log in with the credentials of the remote user "gui".
  
{{< figure class="card__figure" src="/img/debian_xfce_xrdp.png" alt="Screen-shot of XFCE Desktop on Debian 10 (Buster) in a xrdp connection window" title="xrdp connection to a XFCE Desktop on Debian 10 (Buster)" >}}

{{< figure class="card__figure" src="/img/ubuntu_xfce_xrdp.png" alt="Screen-shot of XFCE Desktop on Ubuntu 18.04 in a xrdp connection window" title="xrdp connection to a XFCE Desktop on Ubuntu 18.04" >}}

<script src="/js/prism.js"></script>