---
title: "Tools"
description: "and Docker images"
date: 2019-08-21T23:59:59+01:00
draft: false
---

# All the resources to create and serve this website:

## Container images and configuration files on [gitlab.com/dosmode](https://gitlab.com/dosmode).

All the code is available on [gitlab.com/dosmode](https://gitlab.com/dosmode). The content and configuration for the static site generator Hugo is in [gitlab.com/dosmode/blog](https://gitlab.com/dosmode/blog), the configuration for the backend, i.e. the `Dockerfiles` for the reverse-proxy Traefik and for the nginx web-server can be found in [gitlab.com/dosmode/backend](https://gitlab.com/dosmode/backend), and the tools for creating a local development environment using containers for Hugo and for `fonttools` are in [gitlab.com/dosmode/tools](https://gitlab.com/dosmode/tools).

As I have not implemented a commenting system (yet) you can now use/misuse the [Issues feature](https://gitlab.com/groups/dosmode/-/issues) to make suggestions or voice your critique/criticism.

## Update 2019-08-31: GitLab possibly not very privacy friendly

I do realize that GitLab.com <a href="https://docs.gitlab.com/ee/integration/recaptcha.html">may not be very privacy friendly</a>, and while you can inspect and clone/download all the code, to use the issue tracker you need to register or sign in to create issues for this project – if you don't have an account on GitLab.com or you don't wanna use that for commenting this is actually a pretty poor solution for comments.

Hugo offers a few suggestions to [implement commenting](https://gohugo.io/content-management/comments/#comments-alternatives) on static sites – and you probably wanna [stay away](https://www.kuketz-blog.de/disqus-zentralisierte-diskussionsplattform-oder-datengrab/) from Disqus if you respect the privacy of your visitors. I'll have a look at these suggestions in the future in order to find a better way to implement commenting.