"use strict";

var el_header = document.getElementsByTagName("header")[0];
var bg_img_dist = getComputedStyle(el_header).getPropertyValue(
  "background-image"
);
el_header.style.backgroundBlendMode = "difference";

el_header.onmousedown = function(e) {
  var pos_x = e.clientX;
  var pos_y = e.clientY;
  var el_width = el_header.offsetWidth;
  var el_height = el_header.offsetHeight;
  var rel_x = (pos_x * 100) / el_width;
  var rel_y = (pos_y * 100) / el_height;
  console.log("relative mouse location:", rel_x.toFixed(2), rel_y.toFixed(2));
  var hue = Number.parseInt(rel_x * 3.6);
  var sat = 90;
  var lit = Number.parseInt(rel_y);

  if (pos_y < 45) {
    sat = 0;
    lit = Number.parseInt(rel_x);
  }

  var new_bg_img = "linear-gradient(hsl("
    .concat(hue, ", ")
    .concat(sat, "%, ")
    .concat(lit, "%), hsl(")
    .concat(hue, ", ")
    .concat(sat, "%, ")
    .concat(lit, "%)), ")
    .concat(bg_img_dist);
  console.log("new_bg_img: ", new_bg_img);
  el_header.style.backgroundImage = new_bg_img;
};
